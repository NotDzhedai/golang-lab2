package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"order-service/internal/config"
	"order-service/internal/controller"
	httphandler "order-service/internal/handler/http_handler"
	"order-service/internal/midlwr"
	"order-service/internal/rabbit"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

const ServiceName = "order_service"

func main() {
	// Config
	cfg, err := config.GetConfig("config.yaml")
	if err != nil {
		panic(err)
	}

	// Rabbit setup
	rabbitConn, err := rabbit.Connect(cfg)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := rabbitConn.Close(); err != nil {
			panic(err)
		}
	}()

	emitter, err := rabbit.NewEventEmitter(rabbitConn, cfg)
	if err != nil {
		panic(err)
	}

	ctrl := controller.New(emitter)

	// Handler setup
	h := httphandler.New(ctrl)

	// ! Endpoints
	mux := chi.NewRouter()

	// middleware
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)
	mux.Use(midlwr.CheckAuth)

	mux.Post("/order", h.PlaceOrder)

	// ! End endpoints

	// Server setup
	serverErrors := make(chan error, 1)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", cfg.Port),
		Handler: mux,
	}

	// Run server
	go func() {
		log.Printf("Starting [%s] on port: %s\n", ServiceName, cfg.Port)
		serverErrors <- server.ListenAndServe()
	}()

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	select {
	case err := <-serverErrors: // panic on server error
		panic(err)
	case <-shutdown: // gracefull server shutdown
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		if err := server.Shutdown(ctx); err != nil {
			log.Printf("Shutdown [%s]\n", ServiceName)
			server.Close()
			panic("could not stop storage server gracefully")
		}
	}
}
