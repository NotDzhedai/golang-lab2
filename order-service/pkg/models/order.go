package models

import (
	"github.com/shopspring/decimal"
)

type StoragePosition struct {
	ProductID int `json:"product_id" validate:"required"`
	Quantity  int `json:"quantity" validate:"gte=0"`
}

// Order -> []StoragePosition
type Order []StoragePosition

type OrderResponse struct {
	Total decimal.Decimal `json:"total"`
}

// for returning to user detailed position in storage
type DetailedStoragePosition struct {
	ID          int             `json:"id"`
	Name        string          `json:"name"`
	Description string          `json:"description"`
	Price       decimal.Decimal `json:"price"`
	Quantity    int             `json:"quantity"`
}
