package models

import "time"

type HistoryRecord struct {
	Order  []StoragePosition `json:"order" bson:"order"`
	UserID string            `json:"user_id" bson:"user_id"`
	Total  string            `json:"total" bson:"total"`
	Date   time.Time         `json:"date" bson:"date"`
}

type RabbitPayload struct {
	Name   string        `json:"name"`
	Record HistoryRecord `json:"record"`
}
