package controller

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"order-service/internal/rabbit"
	"order-service/pkg/models"
	"order-service/pkg/web"
	"sync"
	"time"

	"github.com/shopspring/decimal"
)

var ErrDecoding = errors.New("error decoding")
var ErrEncoding = errors.New("error encoding")
var ErrInternal = errors.New("an internal error occured")
var ErrPushToQueue = errors.New("error pushing data to queue")

type Controller struct {
	rabbitEmitter *rabbit.Emitter
}

func New(e *rabbit.Emitter) *Controller {
	return &Controller{
		rabbitEmitter: e,
	}
}

func (c *Controller) pushToQueue(ctx context.Context, payload models.RabbitPayload) error {
	j, err := json.Marshal(payload)
	if err != nil {
		return ErrDecoding
	}

	err = c.rabbitEmitter.Push(ctx, string(j))
	if err != nil {
		return ErrPushToQueue
	}

	return nil
}

func (c *Controller) PlaceOrder(ctx context.Context, order models.Order, sessionID, userID string) (models.OrderResponse, error) {
	// Check order for errors and calculate total price
	total, err := checkOrder(order, sessionID)
	if err != nil {
		return models.OrderResponse{}, err
	}

	// update storage
	if err := updateStorage(order, sessionID); err != nil {
		return models.OrderResponse{}, err
	}

	payload := models.RabbitPayload{
		Name: "save",
		Record: models.HistoryRecord{
			Order:  order,
			UserID: userID,
			Total:  total.String(),
			Date:   time.Now(),
		},
	}
	if err = c.pushToQueue(ctx, payload); err != nil {
		log.Println("Err pushing payload [order_service]: ", err)
	}

	return models.OrderResponse{
		Total: total,
	}, nil
}

func updateStorage(order models.Order, sessionID string) error {
	errChan := make(chan error, len(order))
	wg := &sync.WaitGroup{}

	wg.Add(len(order))
	for _, o := range order {
		go updatePosition(o, sessionID, errChan, wg)
	}

	var allErrors []error

	wg.Wait()

	close(errChan)

	for e := range errChan {
		allErrors = append(allErrors, e)
	}

	if len(allErrors) != 0 {
		var b bytes.Buffer

		for _, err := range allErrors {
			b.WriteString(err.Error() + "\n")
		}

		return web.NewError(
			errors.New(b.String()), http.StatusInternalServerError,
		)
	}

	return nil
}

func updatePosition(
	order models.StoragePosition,
	sessionID string,
	errChan chan error,
	wg *sync.WaitGroup,
) {
	// new data to json
	data, err := json.Marshal(order)
	if err != nil {
		errChan <- ErrEncoding
		wg.Done()
		return
	}

	req, err := http.NewRequest(http.MethodPatch, "http://store-service:3001/storage/sub", bytes.NewBuffer(data))
	if err != nil {
		errChan <- ErrInternal
		wg.Done()
		return
	}
	req.Header.Set("authentication", sessionID)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		errChan <- err
		wg.Done()
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		switch resp.StatusCode {
		case http.StatusNotFound:
			errChan <- fmt.Errorf("product with id [%d] not found", order.ProductID)
			wg.Done()
			return
		default:
			errChan <- ErrInternal
			wg.Done()
			return
		}
	}

	wg.Done()
}

func checkOrder(order models.Order, sessionID string) (decimal.Decimal, error) {
	errChan := make(chan error, len(order))
	totalChan := make(chan decimal.Decimal, len(order))
	wg := &sync.WaitGroup{}

	wg.Add(len(order))
	for _, o := range order {
		go processPosition(o, sessionID, errChan, totalChan, wg)
	}

	var allErrors []error
	var total decimal.Decimal

	wg.Wait()

	close(errChan)
	close(totalChan)

	for e := range errChan {
		allErrors = append(allErrors, e)
	}

	for t := range totalChan {
		total = total.Add(t)
	}

	if len(allErrors) != 0 {
		var b bytes.Buffer

		for _, err := range allErrors {
			b.WriteString(err.Error() + "\n")
		}

		return decimal.Decimal{}, web.NewError(
			errors.New(b.String()), http.StatusBadRequest,
		)
	}

	return total, nil
}

func processPosition(
	order models.StoragePosition,
	sessionID string,
	errChan chan error,
	totalChan chan decimal.Decimal,
	wg *sync.WaitGroup,
) {
	log.Printf("Process order ID: %d, Quantity: %d\n", order.ProductID, order.Quantity)
	// check if such entry exist and enough quantity
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("http://store-service:3001/storage/one?id=%d", order.ProductID), nil)
	if err != nil {
		errChan <- ErrInternal
		wg.Done()
		return
	}
	req.Header.Set("authentication", sessionID)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		errChan <- err
		wg.Done()
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		switch resp.StatusCode {
		case http.StatusNotFound:
			errChan <- fmt.Errorf("product with id [%d] not found", order.ProductID)
			wg.Done()
			return
		default:
			errChan <- ErrInternal
			wg.Done()
			return
		}
	}

	var inStorage models.DetailedStoragePosition
	if err := json.NewDecoder(resp.Body).Decode(&inStorage); err != nil {
		errChan <- web.NewError(ErrDecoding, http.StatusInternalServerError)
		wg.Done()
		return
	}

	// log.Printf("Got position ID: %d, Quantity: %d, Name: %s, Price: %s\n", inStorage.ID, inStorage.Quantity, inStorage.Name, inStorage.Price)

	if inStorage.Quantity < order.Quantity {
		errChan <- web.NewError(
			fmt.Errorf("not enough in stock: %s ", inStorage.Name),
			http.StatusInternalServerError,
		)
		wg.Done()
		return
	}

	// log.Printf("Item in storage ID: %d, Quantity: %d\n", inStorage.ID, inStorage.Quantity)
	// log.Printf("Item in order ID: %d, Quantity: %d\n", order.ProductID, order.Quantity)

	totalPrice := inStorage.Price.Mul(decimal.NewFromInt(int64(order.Quantity)))

	// log.Printf("Total price: %s", totalPrice)

	totalChan <- totalPrice

	wg.Done()
}
