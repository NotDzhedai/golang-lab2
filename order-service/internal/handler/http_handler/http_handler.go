package httphandler

import (
	"encoding/json"
	"net/http"
	"order-service/internal/controller"
	"order-service/pkg/models"
	"order-service/pkg/types"
	"order-service/pkg/web"

	"github.com/go-playground/validator"
)

type Handler struct {
	ctrl      *controller.Controller
	validator *validator.Validate
}

func New(ctrl *controller.Controller) *Handler {
	return &Handler{
		ctrl:      ctrl,
		validator: validator.New(),
	}
}

// Place Order
func (h *Handler) PlaceOrder(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value(types.ContextKey("userID")).(string)
	sessionID := r.Context().Value(types.ContextKey("session_id")).(string)

	var order models.Order

	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		web.RespondWithError(w, "error decoding new position info", http.StatusInternalServerError)
		return
	}

	resp, err := h.ctrl.PlaceOrder(r.Context(), order, sessionID, userID)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	web.Respond(w, resp, http.StatusOK)
}
