package rabbit

import (
	"context"
	"fmt"
	"log"
	"order-service/internal/config"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

func Connect(cfg *config.ApiConfig) (*amqp.Connection, error) {
	var counts int
	var backOff = 2 * time.Second
	var connection *amqp.Connection

	for {
		c, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@rabbitmq", cfg.RabbitUser, cfg.RabbitPassword))
		if err != nil {
			log.Println("RabbitMQ not ready yet...")
			counts++
		} else {
			log.Println("Connected to RabbitMQ")
			connection = c
			break
		}

		if counts > 5 {
			log.Panicln(err)
			return nil, err
		}

		time.Sleep(backOff)
	}

	return connection, nil
}

func declareExchange(ch *amqp.Channel, name string) error {
	return ch.ExchangeDeclare(
		name,
		amqp.ExchangeTopic,
		true,
		false,
		false,
		false,
		nil,
	)
}

type Emitter struct {
	conn         *amqp.Connection
	exchangeName string
}

func NewEventEmitter(conn *amqp.Connection, cfg *config.ApiConfig) (*Emitter, error) {
	e := &Emitter{conn: conn}

	channel, err := e.conn.Channel()
	if err != nil {
		return nil, err
	}

	defer channel.Close()

	e.exchangeName = cfg.RabbitExchangeName

	if err := declareExchange(channel, e.exchangeName); err != nil {
		return nil, err
	}
	return e, nil
}

func (e *Emitter) Push(ctx context.Context, event string) error {
	channel, err := e.conn.Channel()
	if err != nil {
		return err
	}

	defer channel.Close()

	err = channel.PublishWithContext(
		ctx, e.exchangeName, "order", false, false, amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(event),
		},
	)
	if err != nil {
		return err
	}

	return nil
}
