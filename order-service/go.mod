module order-service

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/rabbitmq/amqp091-go v1.8.0
	github.com/shopspring/decimal v1.3.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.2.3 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
