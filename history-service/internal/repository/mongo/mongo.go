package mongo

import (
	"context"
	"fmt"
	"history-service/internal/config"
	"history-service/internal/repository"
	"history-service/pkg/models"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Repository struct {
	client      *mongo.Client
	historyColl *mongo.Collection
	cfg         *config.ApiConfig
}

func New(cfg *config.ApiConfig) (*Repository, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	url := fmt.Sprintf(
		"mongodb://%s:%s@%s:%s",
		cfg.MongoUser,
		cfg.MongoPassword,
		cfg.MongoHost,
		cfg.MongoPort,
	)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(url))
	if err != nil {
		return nil, err
	}

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}

	coll := client.Database(cfg.MongoDatabaseName).Collection("history")

	repo := Repository{
		client:      client,
		cfg:         cfg,
		historyColl: coll,
	}

	return &repo, nil
}

func (r *Repository) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	return r.client.Disconnect(ctx)
}

func (r *Repository) SaveHistoryRecord(ctx context.Context, record models.HistoryRecord) error {
	_, err := r.historyColl.InsertOne(ctx, record)
	if err != nil {
		if mongo.IsDuplicateKeyError(err) {
			return repository.ErrDublicate
		}
		return repository.ErrInternal
	}

	return nil
}
