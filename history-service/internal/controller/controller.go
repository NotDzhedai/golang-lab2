package controller

import (
	"context"
	"history-service/internal/repository/mongo"
	"history-service/pkg/models"
)

type Controller struct {
	repo *mongo.Repository
}

func New(repo *mongo.Repository) *Controller {
	return &Controller{
		repo: repo,
	}
}

func (c *Controller) SaveHistoryRecord(ctx context.Context, record models.HistoryRecord) error {
	return c.repo.SaveHistoryRecord(ctx, record)
}
