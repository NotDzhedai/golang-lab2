package rabbit

import (
	"context"
	"encoding/json"
	"fmt"
	"history-service/internal/config"
	"history-service/internal/controller"
	"history-service/pkg/models"
	"log"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

func Connect(cfg *config.ApiConfig) (*amqp.Connection, error) {
	var counts int
	var backOff = 2 * time.Second
	var connection *amqp.Connection

	for {
		c, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@rabbitmq", cfg.RabbitUser, cfg.RabbitPassword))
		if err != nil {
			log.Println("RabbitMQ not ready yet...")
			counts++
		} else {
			log.Println("Connected to RabbitMQ")
			connection = c
			break
		}

		if counts > 5 {
			log.Panicln(err)
			return nil, err
		}

		time.Sleep(backOff)
	}

	return connection, nil
}

type Consumer struct {
	conn         *amqp.Connection
	exchangeName string
	ctrl         *controller.Controller
}

func NewConsumer(conn *amqp.Connection, ctrl *controller.Controller, cfg *config.ApiConfig) (*Consumer, error) {
	consumer := &Consumer{
		conn: conn,
		ctrl: ctrl,
	}

	channel, err := consumer.conn.Channel()
	if err != nil {
		return nil, err
	}

	consumer.exchangeName = cfg.RabbitExchangeName

	if err = declareExchange(channel, consumer.exchangeName); err != nil {
		return nil, err
	}

	return consumer, nil
}

func declareExchange(ch *amqp.Channel, name string) error {
	return ch.ExchangeDeclare(
		name,
		amqp.ExchangeTopic,
		true,
		false,
		false,
		false,
		nil,
	)
}

func declareRandomQueue(ch *amqp.Channel) (amqp.Queue, error) {
	return ch.QueueDeclare(
		"",
		false,
		false,
		true,
		false,
		nil,
	)
}

func (c *Consumer) Listen() error {
	ch, err := c.conn.Channel()
	if err != nil {
		return nil
	}

	defer ch.Close()

	q, err := declareRandomQueue(ch)
	if err != nil {
		return nil
	}

	err = ch.QueueBind(q.Name, "order", c.exchangeName, false, nil)
	if err != nil {
		return err
	}

	messages, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		return err
	}

	forever := make(chan bool)

	go func() {
		for d := range messages {
			var payload models.RabbitPayload
			err := json.Unmarshal(d.Body, &payload)
			if err != nil {
				log.Printf("[history_service] Error decoding history record: %s\n", err)
			}

			go c.handlePayload(payload)
		}
	}()

	fmt.Println("Waiting for messages on exchange")
	<-forever

	return nil
}

func (c *Consumer) handlePayload(payload models.RabbitPayload) {
	switch payload.Name {
	case "save":
		// save history record
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()
		err := c.ctrl.SaveHistoryRecord(ctx, payload.Record)
		if err != nil {
			log.Printf("[history_service] Error saving history record: %s\n", err)
		}
	default:
		log.Println("Unhandled command to rabbit listener")
	}
}
