package config

import (
	"os"

	"gopkg.in/yaml.v3"
)

type ApiConfig struct {
	Port               string `yaml:"port"`
	MongoUser          string `yaml:"mongouser"`
	MongoPassword      string `yaml:"mongopass"`
	MongoHost          string `yaml:"mongohost"`
	MongoPort          string `yaml:"mongoport"`
	MongoDatabaseName  string `yaml:"mongodatabasename"`
	RabbitUser         string `yaml:"rabbituser"`
	RabbitPassword     string `yaml:"rabbitpassword"`
	RabbitExchangeName string `yaml:"rabbit_exchange_name"`
}

func GetConfig(path string) (*ApiConfig, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	var cfg ApiConfig
	if err := yaml.NewDecoder(f).Decode(&cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
