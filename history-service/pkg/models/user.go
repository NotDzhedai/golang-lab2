package models

type User struct {
	ID       string `json:"id" bson:"id"`
	Nickname string `json:"nickname" bson:"nickname"`
	Email    string `json:"email" bson:"email"`
	Password string `json:"-" bson:"password"`
}
