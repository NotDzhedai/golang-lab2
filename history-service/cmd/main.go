package main

import (
	"history-service/internal/config"
	"history-service/internal/controller"
	"history-service/internal/rabbit"
	"history-service/internal/repository/mongo"
	"log"
)

const ServiceName = "history_service"

func main() {
	// Config
	cfg, err := config.GetConfig("config.yaml")
	if err != nil {
		panic(err)
	}

	// Mongo setup
	log.Printf("[%s] connecting to mongo\n", ServiceName)
	repo, err := mongo.New(cfg)
	if err != nil {
		panic(err)
	}
	log.Printf("[%s] connected to mongo\n", ServiceName)

	defer func() {
		if err := repo.Close(); err != nil {
			panic(err)
		}
	}()

	// Rabbit setup
	rabbitConn, err := rabbit.Connect(cfg)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := rabbitConn.Close(); err != nil {
			panic(err)
		}
	}()

	// ctrl setup
	ctrl := controller.New(repo)

	consumer, err := rabbit.NewConsumer(rabbitConn, ctrl, cfg)
	if err != nil {
		panic(err)
	}

	log.Printf("[%s] listening for events", ServiceName)
	if err := consumer.Listen(); err != nil {
		panic(err)
	}

}
