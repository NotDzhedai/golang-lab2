package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"store-service/cmd/handlers"
	"store-service/internal/config"
	"store-service/internal/controller"
	httphandler "store-service/internal/handler/http_handler"
	"store-service/internal/repository/postgres"
	"syscall"
	"time"
)

const ServiceName = "store_service"

func main() {
	// Config
	cfg, err := config.GetConfig("config.yaml")
	if err != nil {
		panic(err)
	}

	// TODO(@notdzhedai) maybe add consul for tracking

	// Postgres setup
	log.Printf("[%s] connecting to postgres\n", ServiceName)
	repo, err := postgres.New(cfg)
	if err != nil {
		panic(err)
	}
	log.Printf("[%s] connected to postgres\n", ServiceName)

	defer repo.Close()

	// Run migrations
	err = repo.RunMigrations("file:///migrations")
	if err != nil {
		panic(err)
	}

	// Controller setup
	ctrl := controller.New(repo)

	// Handler setup
	h := httphandler.New(ctrl)

	// ! Endpoints
	mux := handlers.NewMux(h)

	// product

	// ! End endpoints

	// Server setup
	serverErrors := make(chan error, 1)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", cfg.Port),
		Handler: mux,
	}

	// Run server
	go func() {
		log.Printf("Starting [%s] on port: %s\n", ServiceName, cfg.Port)
		serverErrors <- server.ListenAndServe()
	}()

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	select {
	case err := <-serverErrors: // panic on server error
		panic(err)
	case <-shutdown: // gracefull server shutdown
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		if err := server.Shutdown(ctx); err != nil {
			log.Printf("Shutdown [%s]\n", ServiceName)
			server.Close()
			panic("could not stop storage server gracefully")
		}
	}
}
