package handlers

import (
	"net/http"
	httphandler "store-service/internal/handler/http_handler"
	"store-service/internal/midlwr"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	redoc "github.com/go-openapi/runtime/middleware"
)

func NewMux(h *httphandler.Handler) http.Handler {
	mux := chi.NewRouter()

	// middleware

	opts := redoc.RedocOpts{
		SpecURL: "/swagger.yaml",
	}
	sh := redoc.Redoc(opts, nil)

	// docs
	mux.Group(func(r chi.Router) {
		r.Handle("/docs", sh)
		r.Handle("/swagger.yaml", http.FileServer(http.Dir("./")))
	})

	// protected
	mux.Group(func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Use(middleware.Recoverer)
		r.Use(midlwr.CheckAuth)

		r.Route("/product", func(r chi.Router) {
			r.Post("/", h.CreateProduct)
			r.Delete("/", h.DeleteProduct)
		})

		// storage
		r.Route("/storage", func(r chi.Router) {
			r.Put("/", h.AddOrUpdateStoragePosition)
			r.Get("/one", h.GetOneDetailedStoragePosition)
			r.Get("/", h.GetAllDetailedStoragePositions)
			r.Delete("/", h.DeleteStoragePosition)
			r.Patch("/add", h.AddQuantityPositionStorage)
			r.Patch("/sub", h.SubtructQuantityPositionStorage)
		})
	})

	return mux
}
