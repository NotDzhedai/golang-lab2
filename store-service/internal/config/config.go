package config

import (
	"os"

	"gopkg.in/yaml.v3"
)

type ApiConfig struct {
	Port       string `yaml:"port"`
	DbUser     string `yaml:"dbuser"`
	DbPassword string `yaml:"dbpassword"`
	DbHost     string `yaml:"dbhost"`
	DbPort     string `yaml:"dbport"`
	DbName     string `yaml:"dbname"`
}

func GetConfig(path string) (*ApiConfig, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	var cfg ApiConfig
	if err := yaml.NewDecoder(f).Decode(&cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
