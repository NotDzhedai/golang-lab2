package controller

import (
	"context"
	"errors"
	"net/http"
	"store-service/internal/repository"
	"store-service/internal/repository/postgres"
	"store-service/pkg/models"
	"store-service/pkg/web"
)

type Controller struct {
	repo *postgres.Repository
}

func New(repo *postgres.Repository) *Controller {
	return &Controller{
		repo: repo,
	}
}

func handleError(err error) error {
	if err == nil {
		return nil
	}

	rErr := &web.ResponseError{}

	switch {
	case errors.Is(err, repository.ErrDublicate):
		rErr.Message = "Such a product already exists"
		rErr.StatusCode = http.StatusBadRequest
		return rErr
	case errors.Is(err, repository.ErrNotFound):
		rErr.Message = "No such product found"
		rErr.StatusCode = http.StatusNotFound
		return rErr
	case errors.Is(err, repository.ErrInternal):
		rErr.Message = "An internal error occurred"
		rErr.StatusCode = http.StatusInternalServerError
		return rErr
	}

	// panic(err)
	return err
}

// Products

// add product R
func (c *Controller) Create(ctx context.Context, np models.NewProduct) (models.Product, error) {
	res, err := c.repo.Create(ctx, np)
	return res, handleError(err)
}

// get product R
func (c *Controller) GetByID(ctx context.Context, id int) (models.Product, error) {
	res, err := c.repo.GetByID(ctx, id)
	return res, handleError(err)
}

// get products R
func (c *Controller) GetAll(ctx context.Context) ([]models.Product, error) {
	res, err := c.repo.GetAll(ctx)
	return res, handleError(err)
}

// delete product D
func (c *Controller) Delete(ctx context.Context, id int) error {
	return handleError(c.repo.Delete(ctx, id))
}

// Storage

// Get storage position from storage
func (c *Controller) getOneStoragePosition(ctx context.Context, id int) (models.StoragePosition, error) {
	res, err := c.repo.GetStoragePosition(ctx, id)
	return res, handleError(err)
}

// Add storage position to storage
func (c *Controller) AddOrUpdateStoragePosition(ctx context.Context, prod models.StoragePosition) (models.DetailedStoragePosition, error) {
	_, err := c.getOneStoragePosition(ctx, prod.ProductID)
	if rErr, ok := err.(*web.ResponseError); ok {
		if rErr.StatusCode == http.StatusNotFound {
			r, err := c.repo.CreateStoragePosition(ctx, prod)
			return r, handleError(err)
		} else {
			return models.DetailedStoragePosition{}, rErr
		}
	}

	// if such entry exists - update with new quantity
	r, err := c.repo.UpdateStoragePosition(ctx, prod)
	return r, handleError(err)
}

// subtruct position's quantity in storage
func (c *Controller) SubtructQuantity(ctx context.Context, change models.StoragePosition) (models.DetailedStoragePosition, error) {
	pos, err := c.getOneStoragePosition(ctx, change.ProductID)
	if err != nil {
		return models.DetailedStoragePosition{}, err
	}

	newQuantity := pos.Quantity - change.Quantity
	if newQuantity < 0 {
		return models.DetailedStoragePosition{}, web.NewError(
			errors.New("quantity can't be negative"), http.StatusBadRequest,
		)
	}

	newData := models.StoragePosition{
		ProductID: pos.ProductID,
		Quantity:  newQuantity,
	}

	r, err := c.repo.UpdateStoragePosition(ctx, newData)
	return r, handleError(err)
}

// add position's quantity in storage
func (c *Controller) AddQuantity(ctx context.Context, change models.StoragePosition) (models.DetailedStoragePosition, error) {
	pos, err := c.getOneStoragePosition(ctx, change.ProductID)
	if err != nil {
		return models.DetailedStoragePosition{}, err
	}

	newQuantity := pos.Quantity + change.Quantity

	newData := models.StoragePosition{
		ProductID: pos.ProductID,
		Quantity:  newQuantity,
	}

	r, err := c.repo.UpdateStoragePosition(ctx, newData)
	return r, handleError(err)
}

// Get storage position detailed from storage
func (c *Controller) GetOneDetailedStoragePosition(ctx context.Context, id int) (models.DetailedStoragePosition, error) {
	res, err := c.repo.GetOneDetailedStoragePosition(ctx, id)
	return res, handleError(err)
}

// Get all storage postitions detailed
func (c *Controller) GetAllDetailedStoragePositions(ctx context.Context) ([]models.DetailedStoragePosition, error) {
	res, err := c.repo.GetAllDetailedStoragePositions(ctx)
	return res, handleError(err)
}

// Delete storage position
func (c *Controller) DeleteStoragePosition(ctx context.Context, id int) error {
	return handleError(c.repo.DeleteStoragePosition(ctx, id))
}
