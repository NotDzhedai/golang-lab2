INSERT INTO products 
    (name, description, price) 
VALUES
    ('apple', 'delicious white apple', 12.99),
    ('pear', 'red juicy pear', 14.99),
    ('banana', 'fresh yellow banana', 18.99)
ON CONFLICT DO NOTHING;


INSERT INTO storage 
    (product_id, quantity)
VALUES
    (1, 20),
    (2, 1),
    (3, 5)
ON CONFLICT DO NOTHING;
