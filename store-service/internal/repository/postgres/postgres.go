package postgres

import (
	"context"
	_ "embed"
	"errors"
	"fmt"
	"log"
	"store-service/internal/config"
	"store-service/internal/repository"
	"store-service/pkg/models"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
)

var (
	//go:embed seed.sql
	seedDoc string
)

type Repository struct {
	conn *pgxpool.Pool
}

func New(cfg *config.ApiConfig) (*Repository, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	dbUrl := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s dbname=%s",
		cfg.DbUser,
		cfg.DbPassword,
		cfg.DbHost,
		cfg.DbPort,
		cfg.DbName,
	)

	conn, err := pgxpool.New(ctx, dbUrl)
	if err != nil {
		return nil, err
	}

	if err = conn.Ping(ctx); err != nil {
		return nil, err
	}

	return &Repository{
		conn: conn,
	}, nil
}

func (r *Repository) RunMigrations(path string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// migration
	m, err := migrate.New(path, "postgres://postgres:secret@postgres:5432/postgres?sslmode=disable")
	if err != nil {
		return err
	}
	m.Up()
	// end migration

	// seed
	tx, err := r.conn.Begin(ctx)
	if err != nil {
		return err
	}

	if _, err := tx.Exec(ctx, seedDoc); err != nil {
		if err := tx.Rollback(ctx); err != nil {
			return err
		}
		return err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return err
	}
	// end seed

	return nil
}

func (r *Repository) Close() {
	r.conn.Close()
}

// helper method
func (r *Repository) exec(ctx context.Context, query string, args ...any) error {
	_, err := r.conn.Exec(ctx, query, args...)
	if err != nil {
		var pErr *pgconn.PgError
		if errors.As(err, &pErr) {
			switch pErr.Code {
			case repository.DUBLICATE_CODE:
				return repository.ErrDublicate
			case repository.FOREIGN_KEY_CODE:
				return repository.ErrNotFound
			}
		}
		return repository.ErrInternal
	}

	return nil
}

// Products

// add product to products table
const createProductQuery = `
	INSERT INTO products
		(name, description, price) 
	VALUES 
		($1, $2, $3)
	RETURNING id, name, description, price
`

func (r *Repository) Create(ctx context.Context, np models.NewProduct) (models.Product, error) {
	var resp models.Product
	if err := r.conn.QueryRow(ctx, createProductQuery, np.Name, np.Description, np.Price).Scan(
		&resp.ID,
		&resp.Name,
		&resp.Description,
		&resp.Price,
	); err != nil {
		var pErr *pgconn.PgError
		if errors.As(err, &pErr) {
			switch pErr.Code {
			case repository.DUBLICATE_CODE:
				return resp, repository.ErrDublicate
			case repository.FOREIGN_KEY_CODE:
				return resp, repository.ErrNotFound
			}
		}
		return resp, repository.ErrInternal
	}

	return resp, nil
}

// get product from products table
const getProductByIDQuery = `
	SELECT 
		id, name, description, price
	FROM
		products
	WHERE
		id = $1
`

// get product by id from products table
func (r *Repository) GetByID(ctx context.Context, id int) (models.Product, error) {
	row := r.conn.QueryRow(ctx, getProductByIDQuery, id)

	var product models.Product
	err := row.Scan(
		&product.ID,
		&product.Name,
		&product.Description,
		&product.Price,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return product, repository.ErrNotFound
		}
		return product, repository.ErrInternal
	}

	return product, nil
}

// get all products from products table
const getAllProductsQuery = `
	SELECT 
		id, name, description, price
	FROM
		products
`

func (r *Repository) GetAll(ctx context.Context) ([]models.Product, error) {
	products := make([]models.Product, 0)

	rows, err := r.conn.Query(ctx, getAllProductsQuery)
	if err != nil {
		return products, repository.ErrInternal
	}
	defer rows.Close()

	for rows.Next() {
		var product models.Product
		if err := rows.Scan(
			&product.ID,
			&product.Name,
			&product.Description,
			&product.Price,
		); err != nil {
			return nil, repository.ErrInternal
		}
		products = append(products, product)
	}

	if err := rows.Err(); err != nil {
		return nil, repository.ErrInternal
	}

	return products, nil
}

// TODO update product in products table

// delete product from products table
const deleteProductQuery = `
	DELETE FROM 
		products
	WHERE
		id = $1
`

func (r *Repository) Delete(ctx context.Context, id int) error {
	t, err := r.conn.Exec(ctx, deleteProductQuery, id)
	if err != nil {
		return repository.ErrInternal
	}

	if t.RowsAffected() == 0 {
		return repository.ErrNotFound
	}

	return nil
}

// Storage

// add position to storage table
const createStoragePosition = `
	INSERT INTO storage 
		(product_id, quantity)
	VALUES
		($1, $2)
`

func (r *Repository) CreateStoragePosition(ctx context.Context, p models.StoragePosition) (models.DetailedStoragePosition, error) {
	if err := r.exec(ctx, createStoragePosition, p.ProductID, p.Quantity); err != nil {
		return models.DetailedStoragePosition{}, err
	}
	return r.GetOneDetailedStoragePosition(ctx, p.ProductID)
}

// update position's quantity in storage table
const updateStoragePositionQuery = `
	UPDATE 
		storage
	SET
		quantity = $1
	WHERE 
		product_id = $2
`

func (r *Repository) UpdateStoragePosition(ctx context.Context, p models.StoragePosition) (models.DetailedStoragePosition, error) {
	if err := r.exec(ctx, updateStoragePositionQuery, p.Quantity, p.ProductID); err != nil {
		return models.DetailedStoragePosition{}, err
	}
	return r.GetOneDetailedStoragePosition(ctx, p.ProductID)
}

// get single position from storage table
// not for public endpoint
const oneStoragePositionQuery = `
	SELECT
		product_id, quantity
	FROM
		storage
	WHERE
		product_id = $1 
`

func (r *Repository) GetStoragePosition(ctx context.Context, id int) (models.StoragePosition, error) {
	row := r.conn.QueryRow(ctx, oneStoragePositionQuery, id)

	var product models.StoragePosition
	err := row.Scan(
		&product.ProductID,
		&product.Quantity,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return product, repository.ErrNotFound
		}
		return product, repository.ErrInternal
	}

	return product, nil
}

// get single detailed position info
// for public endpoint
const oneDetailedStoragePositionQuery = `
	SELECT
		id, name, description, price, quantity
	FROM 
		storage
	JOIN 
		products
	ON
		id = product_id
	WHERE
		id = $1

`

func (r *Repository) GetOneDetailedStoragePosition(ctx context.Context, id int) (models.DetailedStoragePosition, error) {
	row := r.conn.QueryRow(ctx, oneDetailedStoragePositionQuery, id)

	var product models.DetailedStoragePosition
	err := row.Scan(
		&product.ID,
		&product.Name,
		&product.Description,
		&product.Price,
		&product.Quantity,
	)
	if err != nil {
		log.Println("err: ", err.Error())
		if errors.Is(err, pgx.ErrNoRows) {
			return product, repository.ErrNotFound
		}
		return product, repository.ErrInternal
	}

	return product, nil
}

// get all detailed positions
// for public endpoint
const allDetailedStoragePositionQuery = `
	SELECT
		id, name, description, price, quantity
	FROM 
		storage
	JOIN 
		products
	ON
		id = product_id
`

func (r *Repository) GetAllDetailedStoragePositions(ctx context.Context) ([]models.DetailedStoragePosition, error) {
	products := make([]models.DetailedStoragePosition, 0)

	rows, err := r.conn.Query(ctx, allDetailedStoragePositionQuery)
	if err != nil {
		return products, repository.ErrInternal
	}
	defer rows.Close()

	for rows.Next() {
		var product models.DetailedStoragePosition
		if err := rows.Scan(
			&product.ID,
			&product.Name,
			&product.Description,
			&product.Price,
			&product.Quantity,
		); err != nil {
			return nil, repository.ErrInternal
		}
		products = append(products, product)
	}

	if err := rows.Err(); err != nil {
		return nil, repository.ErrInternal
	}

	return products, nil
}

// delete storage position
const deleteStoragePosition = `
	DELETE FROM 
		storage
	WHERE
		product_id = $1
`

func (r *Repository) DeleteStoragePosition(ctx context.Context, id int) error {
	t, err := r.conn.Exec(ctx, deleteStoragePosition, id)
	if err != nil {
		return repository.ErrInternal
	}

	if t.RowsAffected() == 0 {
		return repository.ErrNotFound
	}

	return nil
}
