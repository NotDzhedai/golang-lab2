CREATE TABLE storage (
    product_id INTEGER REFERENCES products (id) ON DELETE CASCADE, 
    quantity INTEGER NOT NULL CHECK(quantity >= 0),
    CONSTRAINT pk PRIMARY KEY (product_id)
);