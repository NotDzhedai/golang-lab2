CREATE TABLE products (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE,
    description VARCHAR NOT NULL,
    price NUMERIC NOT NULL CHECK(price >= 0)
)