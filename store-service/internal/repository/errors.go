package repository

import "errors"

// db error codes
const (
	DUBLICATE_CODE   = "23505"
	FOREIGN_KEY_CODE = "23503"
)

// db errors
var ErrNotFound = errors.New("row not found")
var ErrDublicate = errors.New("such row already exists")
var ErrInternal = errors.New("internal error occured")
