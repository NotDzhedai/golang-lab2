package httphandler

import (
	"store-service/pkg/models"
	"store-service/pkg/web"
)

// swagger:response productResponse
type ProductResponse struct {
	// in: body
	Body models.Product
}

// swagger:response responseError
type ResponseError struct {
	// in: body
	Body web.ResponseError
}

// swagger:response noContent
type NoContent struct{}

// swagger:parameters deleteProduct
// swagger:parameters getOneDetailedStoragePosition
// swagger:parameters deleteStoragePosition
type IdParam struct {
	// product id from products table
	// required: true
	ID int `json:"id"`
}

// swagger:parameters createProduct
type CreateProductBody struct {
	// in: body
	// required: true
	Body models.NewProduct
}

// swagger:response detailedStorageResponse
type DetailedStoragePosition struct {
	// in: body
	Body models.DetailedStoragePosition
}

// swagger:response allDetailedStorageResponse
type AllDetailedStoragePosition struct {
	// in: body
	Body []models.DetailedStoragePosition
}

// swagger:parameters addOrUpdateStoragePosition
// swagger:parameters addQuantityPositionStorage
// swagger:parameters subtructQuantityPositionStorage
type AddOrUpdateStoragePositionBody struct {
	// in: body
	// required: true
	Body models.StoragePosition
}
