// Package classification Storage API
//
// # Documentation for Storage API
// This microservice manages the storage. <br>
// Consists of two tables: products, storage  <br>
// products:  <br>
// id SERIAL PRIMARY KEY,  <br>
// name VARCHAR NOT NULL UNIQUE,  <br>
// description VARCHAR NOT NULL,  <br>
// price NUMERIC NOT NULL CHECK(price >= 0)  <br>
// storage:  <br>
// product_id INTEGER REFERENCES products (id) ON DELETE CASCADE,  <br>
// quantity INTEGER NOT NULL CHECK(quantity >= 0),  <br>
// CONSTRAINT pk PRIMARY KEY (product_id)  <br>
//
// Schemes: http
// BasePath: /
// Version: 1.0.0
//
// swagger:meta
package httphandler

import (
	"encoding/json"
	"net/http"
	"store-service/internal/controller"
	"store-service/pkg/models"
	"store-service/pkg/web"
	"strconv"

	"github.com/go-playground/validator/v10"
)

type Handler struct {
	ctrl      *controller.Controller
	validator *validator.Validate
}

func New(ctrl *controller.Controller) *Handler {
	return &Handler{
		ctrl:      ctrl,
		validator: validator.New(),
	}
}

// swagger:route POST /product products createProduct
// Create new product in products table and return it
// security:
//   - token:
//
// Responses:
// 201: productResponse
// default: responseError
func (h *Handler) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var newProduct models.NewProduct

	if err := json.NewDecoder(r.Body).Decode(&newProduct); err != nil {
		web.RespondWithError(w, "error decoding new product info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(newProduct); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.ctrl.Create(r.Context(), newProduct)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	web.Respond(w, resp, http.StatusCreated)
}

// swagger:route DELETE /product products deleteProduct
// Delete product in products table
// security:
//   - token:
//
// Responses:
// 200: noContent
// default: responseError
func (h *Handler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	if idParam == "" {
		web.RespondWithError(w, "'id' query param is required", http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(idParam)
	if err != nil {
		web.RespondWithError(w, "'id' query param must be int", http.StatusBadRequest)
		return
	}

	if err := h.ctrl.Delete(r.Context(), id); err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Storage

// swagger:route PUT /storage storage addOrUpdateStoragePosition
// Add position to storage if not exist
// Update position to storage if exist
// Returns detailed storage position
// security:
//   - token:
//
// Responses:
// 200: detailedStorageResponse
// default: responseError
func (h *Handler) AddOrUpdateStoragePosition(w http.ResponseWriter, r *http.Request) {
	var prod models.StoragePosition

	if err := json.NewDecoder(r.Body).Decode(&prod); err != nil {
		web.RespondWithError(w, "error decoding new position info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(prod); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.ctrl.AddOrUpdateStoragePosition(r.Context(), prod)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	web.Respond(w, resp, http.StatusOK)
}

// swagger:route PATCH /storage/add storage addQuantityPositionStorage
// Add quantity to product in storage
// Returns detailed storage position
// security:
//   - token:
//
// Responses:
// 200: detailedStorageResponse
// default: responseError
func (h *Handler) AddQuantityPositionStorage(w http.ResponseWriter, r *http.Request) {
	var prod models.StoragePosition

	if err := json.NewDecoder(r.Body).Decode(&prod); err != nil {
		web.RespondWithError(w, "error decoding new position info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(prod); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.ctrl.AddQuantity(r.Context(), prod)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	web.Respond(w, resp, http.StatusOK)
}

// swagger:route PATCH /storage/sub storage subtructQuantityPositionStorage
// Subtruct quantity from product in storage
// Returns detailed storage position
// security:
//   - token:
//
// Responses:
// 200: detailedStorageResponse
// default: responseError
func (h *Handler) SubtructQuantityPositionStorage(w http.ResponseWriter, r *http.Request) {
	var prod models.StoragePosition

	if err := json.NewDecoder(r.Body).Decode(&prod); err != nil {
		web.RespondWithError(w, "error decoding new position info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(prod); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.ctrl.SubtructQuantity(r.Context(), prod)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	web.Respond(w, resp, http.StatusOK)
}

// swagger:route GET /storage/one storage getOneDetailedStoragePosition
// Get one detailed storage position from storage
// security:
//   - token:
//
// Responses:
// 200: detailedStorageResponse
// default: responseError
func (h *Handler) GetOneDetailedStoragePosition(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	if idParam == "" {
		web.RespondWithError(w, "'id' query param is required", http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(idParam)
	if err != nil {
		web.RespondWithError(w, "'id' query param must be int", http.StatusBadRequest)
		return
	}

	product, err := h.ctrl.GetOneDetailedStoragePosition(r.Context(), id)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	if err := web.Respond(w, product, http.StatusOK); err != nil {
		panic(err)
	}
}

// swagger:route GET /storage storage getAllDetailedStoragePositions
// Get all detailed storage positions from storage
// security:
//   - token:
//
// Responses:
// 200: allDetailedStorageResponse
// default: responseError
func (h *Handler) GetAllDetailedStoragePositions(w http.ResponseWriter, r *http.Request) {
	products, err := h.ctrl.GetAllDetailedStoragePositions(r.Context())
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	if err := web.Respond(w, products, http.StatusOK); err != nil {
		panic(err)
	}
}

// swagger:route DELETE /storage storage deleteStoragePosition
// Delete position in storage
// security:
//   - token:
//
// Responses:
// 200: noContent
// default: responseError
func (h *Handler) DeleteStoragePosition(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	if idParam == "" {
		web.RespondWithError(w, "'id' query param is required", http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(idParam)
	if err != nil {
		web.RespondWithError(w, "'id' query param must be int", http.StatusBadRequest)
		return
	}

	if err := h.ctrl.DeleteStoragePosition(r.Context(), id); err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	w.WriteHeader(http.StatusOK)
}

/*
NOT USED

func (h *Handler) GetProductByID(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	if idParam == "" {
		http.Error(w, "'id' query param is required", http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(idParam)
	if err != nil {
		http.Error(w, "'id' query param must be int", http.StatusBadRequest)
		return
	}

	product, err := h.ctrl.GetByID(r.Context(), id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	resp, err := json.Marshal(product)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}


*/
