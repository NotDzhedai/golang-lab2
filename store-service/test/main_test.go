package test

import (
	"log"
	"net/http"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	timeout := time.Duration(2 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}
	_, err := client.Get("http://user-service:3002")
	if err != nil {
		log.Println("Can't connect to user-service")
	}

	m.Run()
}
