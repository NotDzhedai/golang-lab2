package test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"store-service/cmd/handlers"
	"store-service/internal/config"
	"store-service/internal/controller"
	"store-service/internal/repository/postgres"
	"store-service/pkg/models"
	"store-service/pkg/web"
	"strconv"
	"testing"

	httphandler "store-service/internal/handler/http_handler"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

type StoreTest struct {
	mux       http.Handler
	authToken string
}

func TestStoreService(t *testing.T) {
	// login for requests
	payload := struct {
		Nickname string `json:"nickname"`
		Password string `json:"password"`
	}{
		Nickname: "vasya",
		Password: "qwerty",
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	resp, err := http.Post("http://user-service:3002/login", "application/json", bytes.NewBuffer(jsonPayload))
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.Equal(t, resp.StatusCode, http.StatusOK)

	defer resp.Body.Close()

	var login struct {
		Token string `json:"token"`
	}
	err = json.NewDecoder(resp.Body).Decode(&login)
	assert.Nil(t, err)
	assert.NotNil(t, login.Token)

	// Config
	cfg, err := config.GetConfig("../config.yaml")
	assert.Nil(t, err)
	assert.NotNil(t, cfg)

	// Postgres setup
	repo, err := postgres.New(cfg)
	assert.Nil(t, err)
	assert.NotNil(t, repo)

	err = repo.RunMigrations("file://../internal/repository/postgres/migrations")
	assert.Nil(t, err)

	defer repo.Close()

	// Controller setup
	ctrl := controller.New(repo)

	// Handler setup
	h := httphandler.New(ctrl)

	mux := handlers.NewMux(h)

	st := StoreTest{
		mux:       mux,
		authToken: login.Token,
	}

	// test endpoints goes here
	t.Run("create201", st.create201)
	t.Run("create400dublicate", st.create400dublicate)
	t.Run("create400invalid", st.create400invalid)
	t.Run("putStoragePosition200", st.putStoragePosition200)
	t.Run("putStoragePosition400", st.putStoragePosition400)
	t.Run("getOneStoragePosition200", st.getOneStoragePosition200)
	t.Run("getOneStoragePosition404", st.getOneStoragePosition404)
	t.Run("getOneStoragePosition400IDparam", st.getOneStoragePosition400IDparam)
	t.Run("getAllStoragePosition200", st.getAllStoragePosition200)
	t.Run("changeStoragePositionQuantity200", st.changeStoragePositionQuantity200)
	t.Run("subStoragePositionQuantity400negative", st.subStoragePositionQuantity400negative)
	t.Run("deleteStoragePosition200and404", st.deleteStoragePosition200and404)
	t.Run("deleteProduct200and404", st.deleteProduct200and404)
	t.Run("auth403noHeader", st.auth403noHeader)
	t.Run("auth403invalidToken", st.auth403invalidToken)
}

func (st *StoreTest) auth403noHeader(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/storage", nil)
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusForbidden, w.Code)
	var got web.ResponseError
	err := json.NewDecoder(w.Body).Decode(&got)
	assert.Nil(t, err)
	assert.NotNil(t, got)
	assert.Equal(t, http.StatusForbidden, got.StatusCode)
	assert.Equal(t, "'authentication' header is required", got.Message)
}

func (st *StoreTest) auth403invalidToken(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/storage", nil)
	r.Header.Set("authentication", "INVALID TOKEN")
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusForbidden, w.Code)
	var got web.ResponseError
	err := json.NewDecoder(w.Body).Decode(&got)
	assert.Nil(t, err)
	assert.NotNil(t, got)
	assert.Equal(t, http.StatusForbidden, got.StatusCode)
	assert.Equal(t, "provided token is not valid (maybe expired)", got.Message)
}

// helper method for creating product
func (st *StoreTest) createProduct(t *testing.T, name, desc, price string) (*httptest.ResponseRecorder, decimal.Decimal) {
	p, err := decimal.NewFromString(price)
	assert.Nil(t, err)
	assert.NotNil(t, price)

	payload := models.NewProduct{
		Name:        name,
		Description: desc,
		Price:       p,
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r := httptest.NewRequest(http.MethodPost, "/product", bytes.NewBuffer(jsonPayload))
	r.Header.Set("authentication", st.authToken)
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	return w, p
}

func (st *StoreTest) create201(t *testing.T) {
	w, price := st.createProduct(t, "fanta", "desc", "19.99")

	assert.Equal(t, http.StatusCreated, w.Code)

	var result models.Product
	err := json.NewDecoder(w.Body).Decode(&result)
	assert.Nil(t, err)

	assert.Equal(t, "fanta", result.Name)
	assert.Equal(t, "desc", result.Description)
	c := price.Cmp(result.Price)
	assert.Equal(t, 0, c)

	assert.NotNil(t, result.ID)
}

func (st *StoreTest) create400dublicate(t *testing.T) {
	st.createProduct(t, "coca-cola", "desc", "13.99")
	// create duclicate
	w, _ := st.createProduct(t, "coca-cola", "desc", "13.99")

	assert.Equal(t, http.StatusBadRequest, w.Code)

	var result web.ResponseError
	err := json.NewDecoder(w.Body).Decode(&result)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, result.StatusCode)
	assert.Equal(t, "Such a product already exists", result.Message)
}

func (st *StoreTest) create400invalid(t *testing.T) {
	w, _ := st.createProduct(t, "", "desc", "4.44")

	assert.Equal(t, http.StatusBadRequest, w.Code)

	var result web.ResponseError
	err := json.NewDecoder(w.Body).Decode(&result)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, result.StatusCode)
}

// helper method for creating storage position
func (st *StoreTest) putStoragePosition(t *testing.T, productID, quantity int) *httptest.ResponseRecorder {
	payload := models.StoragePosition{
		ProductID: productID,
		Quantity:  quantity,
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r := httptest.NewRequest(http.MethodPut, "/storage", bytes.NewBuffer(jsonPayload))
	r.Header.Set("authentication", st.authToken)
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	return w
}

func (st *StoreTest) putStoragePosition200(t *testing.T) {
	w, price := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// Create
	w = st.putStoragePosition(t, product.ID, 100)

	assert.Equal(t, http.StatusOK, w.Code)
	var positionCreated models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&positionCreated)
	assert.Nil(t, err)

	assert.Equal(t, product.ID, positionCreated.ID)
	assert.Equal(t, product.Description, positionCreated.Description)
	assert.Equal(t, product.Name, positionCreated.Name)
	c := price.Cmp(positionCreated.Price)
	assert.Equal(t, 0, c)
	assert.Equal(t, 100, positionCreated.Quantity)

	// Update
	w = st.putStoragePosition(t, product.ID, 244)
	assert.Equal(t, http.StatusOK, w.Code)
	var positionUpdated models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&positionUpdated)
	assert.Nil(t, err)

	assert.Equal(t, product.ID, positionUpdated.ID)
	assert.Equal(t, product.Description, positionUpdated.Description)
	assert.Equal(t, product.Name, positionUpdated.Name)
	c = price.Cmp(positionUpdated.Price)
	assert.Equal(t, 0, c)
	assert.Equal(t, 244, positionUpdated.Quantity)
}

func (st *StoreTest) putStoragePosition400(t *testing.T) {
	w, _ := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// Create
	w = st.putStoragePosition(t, product.ID, -42)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func (st *StoreTest) getOneStoragePosition200(t *testing.T) {
	// create product
	w, _ := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// create position
	w = st.putStoragePosition(t, product.ID, 100)
	assert.Equal(t, http.StatusOK, w.Code)
	var position models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&position)
	assert.Nil(t, err)

	// get position
	idParam := strconv.Itoa(position.ID)
	r := httptest.NewRequest(http.MethodGet, "/storage/one?id="+idParam, nil)
	r.Header.Set("authentication", st.authToken)
	// err = r.
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	var got models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&got)
	assert.Nil(t, err)

	// check got position
	assert.Equal(t, position.ID, got.ID)
	assert.Equal(t, position.Description, got.Description)
	assert.Equal(t, position.Name, got.Name)
	c := position.Price.Cmp(got.Price)
	assert.Equal(t, 0, c)
	assert.Equal(t, 100, got.Quantity)
}

func (st *StoreTest) getOneStoragePosition404(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/storage/one?id=4234", nil)
	r.Header.Set("authentication", st.authToken)
	// err = r.
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func (st *StoreTest) getOneStoragePosition400IDparam(t *testing.T) {
	// required
	r := httptest.NewRequest(http.MethodGet, "/storage/one", nil)
	r.Header.Set("authentication", st.authToken)
	// err = r.
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusBadRequest, w.Code)
	var err1 web.ResponseError
	err := json.NewDecoder(w.Body).Decode(&err1)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, err1.StatusCode)
	assert.Equal(t, "'id' query param is required", err1.Message)

	// must be int
	r = httptest.NewRequest(http.MethodGet, "/storage/one?id=fwefwe", nil)
	r.Header.Set("authentication", st.authToken)
	// err = r.
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusBadRequest, w.Code)
	var err2 web.ResponseError
	err = json.NewDecoder(w.Body).Decode(&err2)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, err2.StatusCode)
	assert.Equal(t, "'id' query param must be int", err2.Message)
}

func (st *StoreTest) getAllStoragePosition200(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/storage", nil)
	r.Header.Set("authentication", st.authToken)
	// err = r.
	w := httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	var got []models.DetailedStoragePosition
	err := json.NewDecoder(w.Body).Decode(&got)
	assert.Nil(t, err)

	assert.NotNil(t, got)
}

func (st *StoreTest) changeStoragePositionQuantity200(t *testing.T) {
	w, _ := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// Create
	w = st.putStoragePosition(t, product.ID, 100)

	assert.Equal(t, http.StatusOK, w.Code)
	var positionCreated models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&positionCreated)
	assert.Nil(t, err)

	// Add quantity
	payload := models.StoragePosition{
		ProductID: positionCreated.ID,
		Quantity:  50,
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r := httptest.NewRequest(http.MethodPatch, "/storage/add", bytes.NewBuffer(jsonPayload))
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	var positionUpdated models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&positionUpdated)
	assert.Nil(t, err)

	assert.Equal(t, 150, positionUpdated.Quantity)

	// Subtract quantity
	payload = models.StoragePosition{
		ProductID: positionCreated.ID,
		Quantity:  100,
	}

	jsonPayload, err = json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r = httptest.NewRequest(http.MethodPatch, "/storage/sub", bytes.NewBuffer(jsonPayload))
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	var got models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&got)
	assert.Nil(t, err)

	assert.Equal(t, 50, got.Quantity)
}

func (st *StoreTest) subStoragePositionQuantity400negative(t *testing.T) {
	w, _ := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// Create
	w = st.putStoragePosition(t, product.ID, 100)

	assert.Equal(t, http.StatusOK, w.Code)
	var positionCreated models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&positionCreated)
	assert.Nil(t, err)

	// sub negative test
	payload := models.StoragePosition{
		ProductID: positionCreated.ID,
		Quantity:  positionCreated.Quantity + 1,
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r := httptest.NewRequest(http.MethodPatch, "/storage/sub", bytes.NewBuffer(jsonPayload))
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusBadRequest, w.Code)
	var rErr web.ResponseError
	err = json.NewDecoder(w.Body).Decode(&rErr)
	assert.Nil(t, err)

	assert.Equal(t, http.StatusBadRequest, rErr.StatusCode)
	assert.Equal(t, "quantity can't be negative", rErr.Message)
}

func (st *StoreTest) deleteStoragePosition200and404(t *testing.T) {
	w, _ := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// Create
	w = st.putStoragePosition(t, product.ID, 100)

	assert.Equal(t, http.StatusOK, w.Code)
	var positionCreated models.DetailedStoragePosition
	err = json.NewDecoder(w.Body).Decode(&positionCreated)
	assert.Nil(t, err)

	// Delete
	idParam := strconv.Itoa(positionCreated.ID)
	r := httptest.NewRequest(http.MethodDelete, "/storage?id="+idParam, nil)
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	// Check if deleted
	// delete return 404 if row to delete not found
	r = httptest.NewRequest(http.MethodDelete, "/storage?id="+idParam, nil)
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func (st *StoreTest) deleteProduct200and404(t *testing.T) {
	w, _ := st.createProduct(t, uuid.NewString(), "desc", "4.53")
	assert.Equal(t, http.StatusCreated, w.Code)
	var product models.Product
	err := json.NewDecoder(w.Body).Decode(&product)
	assert.Nil(t, err)

	// Delete
	idParam := strconv.Itoa(product.ID)
	r := httptest.NewRequest(http.MethodDelete, "/product?id="+idParam, nil)
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	// Check if deleted
	// delete return 404 if row to delete not found
	r = httptest.NewRequest(http.MethodDelete, "/product?id="+idParam, nil)
	r.Header.Set("authentication", st.authToken)
	w = httptest.NewRecorder()

	st.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusNotFound, w.Code)
}
