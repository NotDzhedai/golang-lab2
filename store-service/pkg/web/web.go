package web

import (
	"encoding/json"
	"net/http"
)

// swagger:model
type ResponseError struct {
	// required: true
	StatusCode int `json:"status_code"`
	// required: true
	Message string `json:"message"`
}

func (err *ResponseError) Error() string {
	return err.Message
}

func NewError(err error, statusCode int) *ResponseError {
	return &ResponseError{Message: err.Error(), StatusCode: statusCode}
}

func Respond(w http.ResponseWriter, data any, statusCode int) error {
	resp, err := json.Marshal(data)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_, err = w.Write(resp)
	return err
}

func RespondWithError(w http.ResponseWriter, message string, statusCode int) error {
	rErr := ResponseError{Message: message, StatusCode: statusCode}
	return Respond(w, rErr, statusCode)
}
