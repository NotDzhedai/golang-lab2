package models

import (
	"github.com/shopspring/decimal"
)

// swagger:model
type Product struct {
	// product id (products table)
	// required: true
	ID int `json:"id"`
	// required: true
	Name string `json:"name"`
	// required: true
	Description string `json:"description"`
	// product Price (decimal.Decimal)
	// required: true
	// min: 0
	Price decimal.Decimal `json:"price"`
}

// swagger:model
type NewProduct struct {
	// required: true
	Name string `json:"name" validate:"required"`
	// required: true
	Description string `json:"description" validate:"required"`
	// product Price (decimal.Decimal)
	// required: true
	// min: 0
	Price decimal.Decimal `json:"price" validate:"gte=0"`
}

// swagger:model
type StoragePosition struct {
	// product id from products table
	// required: true
	ProductID int `json:"product_id" validate:"required"`
	// required: true
	// min: 0
	Quantity int `json:"quantity" validate:"gte=0"`
}

// swagger:model
type DetailedStoragePosition struct {
	// Product
	Product
	// required: true
	// min: 0
	Quantity int `json:"quantity"`
}
