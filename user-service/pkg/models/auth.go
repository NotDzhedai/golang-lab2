package models

type LoginRequest struct {
	Nickname string `json:"nickname" validate:"required,min=5"`
	Password string `json:"password" validate:"required,min=5"`
}

type LoginResponse struct {
	Token string `json:"token"`
}

type IsLoggedInResponse struct {
	UserID     string `json:"user_id,omitempty"`
	IsLoggedIn bool   `json:"is_logged_in"`
}
