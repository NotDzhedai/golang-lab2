package models

type User struct {
	ID       string `json:"id" bson:"id"`
	Nickname string `json:"nickname" bson:"nickname"`
	Email    string `json:"email" bson:"email"`
	Password string `json:"-" bson:"password"`
}

type NewUser struct {
	Nickname string `json:"nickname" validate:"required,min=5"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=5"`
}

type UpdateUser struct {
	Nickname *string `json:"nickname" validate:"omitempty,min=5"`
	Email    *string `json:"email" validate:"omitempty,email"`
}
