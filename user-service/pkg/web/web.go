package web

import (
	"encoding/json"
	"net/http"
)

type ResponseError struct {
	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
}

func NewError(err error, statusCode int) *ResponseError {
	return &ResponseError{Message: err.Error(), StatusCode: statusCode}
}

func (err *ResponseError) Error() string {
	return err.Message
}

func Respond(w http.ResponseWriter, data any, statusCode int) error {
	resp, err := json.Marshal(data)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_, err = w.Write(resp)
	return err
}

func RespondWithError(w http.ResponseWriter, message string, statusCode int) error {
	rErr := ResponseError{Message: message, StatusCode: statusCode}
	return Respond(w, rErr, statusCode)
}
