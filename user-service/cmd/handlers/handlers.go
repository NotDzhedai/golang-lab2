package handlers

import (
	"net/http"
	httphandler "user-service/internal/handler/http_handler"
	"user-service/internal/midlwr"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func NewMux(h *httphandler.Handler) http.Handler {
	mux := chi.NewRouter()

	// middleware
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)

	// User
	mux.Post("/user", h.CreateUser)
	mux.Get("/user", h.GetUserByNickname)

	// Protected routes
	mux.Group(func(r chi.Router) {
		r.Use(midlwr.CheckAuth)
		r.Delete("/user", h.DeleteUser)
		r.Put("/user", h.UpdateUser)
	})

	// Login
	mux.Post("/login", h.Login)
	mux.Get("/isloggedin", h.IsLoggedIn)

	return mux
}
