package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"user-service/cmd/handlers"
	"user-service/internal/auth"
	"user-service/internal/config"
	"user-service/internal/controller"
	httphandler "user-service/internal/handler/http_handler"
	"user-service/internal/repository/mongo"
)

const ServiceName = "user_service"

func main() {
	// Config
	cfg, err := config.GetConfig("config.yaml")
	if err != nil {
		panic(err)
	}

	// Mongo setup
	log.Printf("[%s] connecting to mongo\n", ServiceName)
	repo, err := mongo.New(cfg)
	if err != nil {
		panic(err)
	}
	log.Printf("[%s] connected to mongo\n", ServiceName)

	defer func() {
		if err := repo.Close(); err != nil {
			panic(err)
		}
	}()

	// Auth service setup
	log.Printf("[%s] connecting to redis\n", ServiceName)
	authService, err := auth.New(cfg)
	if err != nil {
		panic(err)
	}
	log.Printf("[%s] connected to redis\n", ServiceName)

	// Controller setup
	ctrl := controller.New(repo, authService)

	// Handler setup
	h := httphandler.New(ctrl)

	// ! Endpoints
	mux := handlers.NewMux(h)

	// ! End endpoints

	// Server setup
	serverErrors := make(chan error, 1)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", cfg.Port),
		Handler: mux,
	}

	// Run server
	go func() {
		log.Printf("Starting [%s] on port: %s\n", ServiceName, cfg.Port)
		serverErrors <- server.ListenAndServe()
	}()

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	select {
	case err := <-serverErrors: // panic on server error
		panic(err)
	case <-shutdown: // gracefull server shutdown
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		if err := server.Shutdown(ctx); err != nil {
			log.Printf("Shutdown [%s]\n", ServiceName)
			server.Close()
			panic("could not stop storage server gracefully")
		}
	}

}
