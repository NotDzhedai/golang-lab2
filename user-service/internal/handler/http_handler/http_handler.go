package httphandler

import (
	"encoding/json"
	"net/http"
	"user-service/internal/controller"
	"user-service/pkg/models"
	"user-service/pkg/types"
	"user-service/pkg/web"

	"github.com/go-playground/validator"
)

type Handler struct {
	ctrl      *controller.Controller
	validator *validator.Validate
}

func New(ctrl *controller.Controller) *Handler {
	return &Handler{
		ctrl:      ctrl,
		validator: validator.New(),
	}
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var loginRequest models.LoginRequest

	if err := json.NewDecoder(r.Body).Decode(&loginRequest); err != nil {
		web.RespondWithError(w, "error decoding login info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(loginRequest); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	login, err := h.ctrl.Login(r.Context(), loginRequest)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	if err := web.Respond(w, login, http.StatusOK); err != nil {
		panic(err)
	}
}

func (h *Handler) IsLoggedIn(w http.ResponseWriter, r *http.Request) {
	sessionID := r.URL.Query().Get("session_id")
	if sessionID == "" {
		web.RespondWithError(w, "'session_id' query param is required", http.StatusBadRequest)
		return
	}

	user, err := h.ctrl.IsLoggedIn(r.Context(), sessionID)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	if err := web.Respond(w, user, http.StatusOK); err != nil {
		panic(err)
	}
}

func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	var newUser models.NewUser

	if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
		web.RespondWithError(w, "error decoding new user info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(newUser); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	if err := h.ctrl.Create(r.Context(), newUser); err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (h *Handler) GetUserByNickname(w http.ResponseWriter, r *http.Request) {
	nickname := r.URL.Query().Get("nickname")
	if nickname == "" {
		web.RespondWithError(w, "'nickname' query param is required", http.StatusBadRequest)
		return
	}

	user, err := h.ctrl.GetByNickname(r.Context(), nickname)
	if err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	if err := web.Respond(w, user, http.StatusOK); err != nil {
		panic(err)
	}
}

func (h *Handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value(types.ContextKey("userID")).(string)

	if err := h.ctrl.Delete(r.Context(), userID); err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value(types.ContextKey("userID")).(string)

	var updateUser models.UpdateUser

	if err := json.NewDecoder(r.Body).Decode(&updateUser); err != nil {
		web.RespondWithError(w, "error decoding new user info", http.StatusInternalServerError)
		return
	}

	if err := h.validator.Struct(updateUser); err != nil {
		valErrors := err.(validator.ValidationErrors)
		web.RespondWithError(w, valErrors.Error(), http.StatusBadRequest)
		return
	}

	if err := h.ctrl.Update(r.Context(), userID, updateUser); err != nil {
		rErr := err.(*web.ResponseError)
		web.Respond(w, rErr, rErr.StatusCode)
		return
	}

	w.WriteHeader(http.StatusOK)
}
