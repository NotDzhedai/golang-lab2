package midlwr

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"user-service/pkg/models"
	"user-service/pkg/types"
	"user-service/pkg/web"
)

func CheckAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sessionID := r.Header.Get("authentication")
		if sessionID == "" {
			web.RespondWithError(w, "'authentication' header is required", http.StatusForbidden)
			return
		}

		req, err := http.Get(fmt.Sprintf("http://user-service:3002/isloggedin?session_id=%s", sessionID))
		if err != nil {
			web.RespondWithError(w, "internal error during check auth request", http.StatusInternalServerError)
			return
		}
		defer req.Body.Close()

		if req.StatusCode != http.StatusOK {
			web.RespondWithError(w, "provided token is not valid (maybe expired)", http.StatusForbidden)
			return
		}

		var user models.User
		if err := json.NewDecoder(req.Body).Decode(&user); err != nil {
			web.RespondWithError(w, "error decoding user info from token", http.StatusInternalServerError)
			return
		}

		ctx := context.WithValue(r.Context(), types.ContextKey("userID"), user.ID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
