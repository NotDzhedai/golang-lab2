package controller

import (
	"context"
	"errors"
	"net/http"
	"user-service/internal/auth"
	"user-service/internal/repository"
	"user-service/internal/repository/mongo"
	"user-service/pkg/models"
	"user-service/pkg/password"
	"user-service/pkg/web"

	"github.com/google/uuid"
)

var ErrWrongCredentials = errors.New("wrong credentials")
var ErrInternal = errors.New("an internal error occurred while creating the user")
var ErrNotFoundSession = errors.New("not found such session")
var ErrUpdateWithSameValues = errors.New("update user with old values")

type Controller struct {
	repo *mongo.Repository
	auth *auth.AuthService
}

func New(repo *mongo.Repository, auth *auth.AuthService) *Controller {
	return &Controller{
		repo: repo,
		auth: auth,
	}
}

func handleDBError(err error) error {
	if err == nil {
		return nil
	}

	rErr := &web.ResponseError{}

	switch {
	case errors.Is(err, repository.ErrDublicate):
		rErr.Message = "Such a user already exists"
		rErr.StatusCode = http.StatusBadRequest
		return rErr
	case errors.Is(err, repository.ErrNotFound):
		rErr.Message = "No such user found"
		rErr.StatusCode = http.StatusNotFound
		return rErr
	case errors.Is(err, repository.ErrInternal):
		rErr.Message = "An internal error occurred while creating the user"
		rErr.StatusCode = http.StatusInternalServerError
		return rErr
	}

	// panic(err)
	return err
}

func (c *Controller) Login(ctx context.Context, loginRequest models.LoginRequest) (models.LoginResponse, error) {
	user, err := c.GetByNickname(ctx, loginRequest.Nickname)
	if err != nil {
		return models.LoginResponse{}, handleDBError(err)
	}

	if err := password.CheckPassword(loginRequest.Password, user.Password); err != nil {
		return models.LoginResponse{}, web.NewError(ErrWrongCredentials, http.StatusBadRequest)
	}

	token, err := c.auth.AddSession(ctx, user.ID)
	if err != nil {
		return models.LoginResponse{}, web.NewError(err, http.StatusInternalServerError)
	}

	resp := models.LoginResponse{
		Token: token,
	}

	return resp, nil
}

func (c *Controller) IsLoggedIn(ctx context.Context, sessionID string) (models.User, error) {
	userID, ok := c.auth.CheckSession(ctx, sessionID)
	if !ok {
		return models.User{}, web.NewError(
			ErrWrongCredentials,
			http.StatusNotFound,
		)
	}

	return c.GetByID(ctx, userID)
}

func (c *Controller) Create(ctx context.Context, newUser models.NewUser) error {
	// create final user struct
	user := models.User{}
	user.Email = newUser.Email
	user.Nickname = newUser.Nickname
	user.ID = uuid.NewString()

	hashedPassword, err := password.HashPassword(newUser.Password)
	if err != nil {
		return web.NewError(ErrInternal, http.StatusInternalServerError)
	}

	user.Password = hashedPassword

	return handleDBError(c.repo.Create(ctx, user))
}

func (c *Controller) GetByNickname(ctx context.Context, nickname string) (models.User, error) {
	r, err := c.repo.GetByNickname(ctx, nickname)
	return r, handleDBError(err)
}

func (c *Controller) GetByID(ctx context.Context, id string) (models.User, error) {
	r, err := c.repo.GetByID(ctx, id)
	return r, handleDBError(err)
}

func (c *Controller) Delete(ctx context.Context, id string) error {
	return handleDBError(c.repo.Delete(ctx, id))
}

func (c *Controller) Update(ctx context.Context, id string, updateUser models.UpdateUser) error {
	user, err := c.GetByID(ctx, id)
	if err != nil {
		return err
	}

	if updateUser.Email != nil {
		if *updateUser.Email == user.Email {
			return web.NewError(ErrUpdateWithSameValues, http.StatusBadRequest)
		}
		user.Email = *updateUser.Email
	}
	if updateUser.Nickname != nil {
		if *updateUser.Nickname == user.Nickname {
			return web.NewError(ErrUpdateWithSameValues, http.StatusBadRequest)
		}
		user.Nickname = *updateUser.Nickname
	}

	return handleDBError(c.repo.Update(ctx, user))
}
