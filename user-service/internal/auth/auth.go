package auth

import (
	"context"
	"errors"
	"fmt"
	"time"
	"user-service/internal/config"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

var ErrAuth = errors.New("error while creating session")

type AuthService struct {
	redisClient *redis.Client
}

func New(cfg *config.ApiConfig) (*AuthService, error) {
	// Redis setup
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort),
		Password: cfg.RedisPassword,
		DB:       cfg.RedisDB,
	})

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := client.Ping(ctx).Result()
	if err != nil {
		return nil, fmt.Errorf("error ping redis: %s", err)
	}

	return &AuthService{
		redisClient: client,
	}, nil
}

func (as *AuthService) AddSession(ctx context.Context, userID string) (string, error) {
	key := uuid.NewString()
	timeDur := 5 * time.Minute
	if err := as.redisClient.Set(ctx, key, userID, timeDur).Err(); err != nil {
		return "", err
	}

	return key, nil
}

func (as *AuthService) CheckSession(ctx context.Context, sessionID string) (string, bool) {
	val, err := as.redisClient.Get(ctx, sessionID).Result()
	if err != nil {
		return "", false
	}

	return val, true
}
