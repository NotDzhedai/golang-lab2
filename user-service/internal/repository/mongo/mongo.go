package mongo

import (
	"context"
	"fmt"
	"time"
	"user-service/internal/config"
	"user-service/internal/repository"
	"user-service/pkg/models"
	"user-service/pkg/password"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Repository struct {
	client    *mongo.Client
	usersColl *mongo.Collection
	cfg       *config.ApiConfig
}

func New(cfg *config.ApiConfig) (*Repository, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	url := fmt.Sprintf(
		"mongodb://%s:%s@%s:%s",
		cfg.MongoUser,
		cfg.MongoPassword,
		cfg.MongoHost,
		cfg.MongoPort,
	)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(url))
	if err != nil {
		return nil, err
	}

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}

	// Set unique email and nickname on users
	coll := client.Database(cfg.MongoDatabaseName).Collection("users")
	_, err = coll.Indexes().CreateMany(ctx, []mongo.IndexModel{
		{
			Keys: bson.M{
				"email": 1,
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.M{
				"nickname": 1,
			},
			Options: options.Index().SetUnique(true),
		},
	})
	if err != nil {
		return nil, err
	}

	repo := Repository{
		client:    client,
		cfg:       cfg,
		usersColl: coll,
	}

	// ! Seed data
	pass, _ := password.HashPassword("qwerty")
	repo.Create(ctx, models.User{
		ID:       uuid.NewString(),
		Nickname: "vasya",
		Email:    "vasya@gmail.com",
		Password: pass,
	})

	return &repo, nil
}

func (r *Repository) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	return r.client.Disconnect(ctx)
}

// Create user
func (r *Repository) Create(ctx context.Context, user models.User) error {
	_, err := r.usersColl.InsertOne(ctx, user)
	if err != nil {
		if mongo.IsDuplicateKeyError(err) {
			return repository.ErrDublicate
		}
		return repository.ErrInternal
	}

	return nil
}

// Get user by nickname
func (r *Repository) GetByNickname(ctx context.Context, nickname string) (models.User, error) {
	result := r.usersColl.FindOne(ctx, bson.M{"nickname": nickname})

	var user models.User
	if err := result.Decode(&user); err != nil {
		if err == mongo.ErrNoDocuments {
			return models.User{}, repository.ErrNotFound
		}
		return models.User{}, repository.ErrInternal
	}

	return user, nil
}

// Get user by id
func (r *Repository) GetByID(ctx context.Context, id string) (models.User, error) {
	result := r.usersColl.FindOne(ctx, bson.M{"id": id})

	var user models.User
	if err := result.Decode(&user); err != nil {
		if err == mongo.ErrNoDocuments {
			return models.User{}, repository.ErrNotFound
		}
		return models.User{}, repository.ErrInternal
	}

	return user, nil
}

// Delete user
func (r *Repository) Delete(ctx context.Context, id string) error {
	result, err := r.usersColl.DeleteOne(ctx, bson.M{"id": id})
	if err != nil {
		return repository.ErrInternal
	}

	if result.DeletedCount == 0 {
		return repository.ErrNotFound
	}

	return nil
}

// Update user
func (r *Repository) Update(ctx context.Context, user models.User) error {
	_, err := r.usersColl.ReplaceOne(ctx, bson.M{"id": user.ID}, user)
	if err != nil {
		return repository.ErrInternal
	}

	return nil
}
