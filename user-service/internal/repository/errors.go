package repository

import "errors"

// db errors
var ErrNotFound = errors.New("user not found")
var ErrDublicate = errors.New("such user already exists")
var ErrInternal = errors.New("internal error occured")
