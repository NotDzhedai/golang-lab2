package test

/*
	GOVNO KOD
*/

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-service/cmd/handlers"
	"user-service/internal/auth"
	"user-service/internal/config"
	"user-service/internal/controller"
	httphandler "user-service/internal/handler/http_handler"
	"user-service/internal/repository/mongo"
	"user-service/pkg/models"

	"github.com/stretchr/testify/assert"
)

type UserTest struct {
	mux http.Handler
}

func TestUserService(t *testing.T) {

	// Config
	cfg, err := config.GetConfig("../config.yaml")
	assert.Nil(t, err)
	assert.NotNil(t, cfg)

	a, err := auth.New(cfg)
	assert.Nil(t, err)

	repo, err := mongo.New(cfg)
	assert.Nil(t, err)

	// Controller setup
	ctrl := controller.New(repo, a)

	// Handler setup
	h := httphandler.New(ctrl)

	mux := handlers.NewMux(h)

	ut := UserTest{}

	ut.mux = mux

	t.Run("create201", ut.create201)
	t.Run("getByNickname", ut.getByNickname)
}

func (ut *UserTest) create201(t *testing.T) {
	payload := models.NewUser{
		Nickname: "teeeeest",
		Email:    "teeeeest@gmail.com",
		Password: "qwerty",
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r := httptest.NewRequest(http.MethodPost, "/user", bytes.NewBuffer(jsonPayload))
	w := httptest.NewRecorder()

	ut.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusCreated, w.Code)
}

func (ut *UserTest) getByNickname(t *testing.T) {
	// create
	payload := models.NewUser{
		Nickname: "teeeeest1",
		Email:    "teeeeest1@gmail.com",
		Password: "qwerty",
	}

	jsonPayload, err := json.Marshal(payload)
	assert.Nil(t, err)
	assert.NotNil(t, jsonPayload)

	r := httptest.NewRequest(http.MethodPost, "/user", bytes.NewBuffer(jsonPayload))
	w := httptest.NewRecorder()

	ut.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusCreated, w.Code)

	// get
	r = httptest.NewRequest(http.MethodGet, "/user?nickname="+payload.Nickname, bytes.NewBuffer(jsonPayload))
	w = httptest.NewRecorder()

	ut.mux.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	var got models.User
	err = json.NewDecoder(w.Body).Decode(&got)
	assert.Nil(t, err)

	assert.Equal(t, payload.Nickname, got.Nickname)
	assert.Equal(t, payload.Email, got.Email)
	assert.NotNil(t, got.ID)
	assert.NotNil(t, got.Password)
}
