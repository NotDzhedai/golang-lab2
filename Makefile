docker-test-store:
	docker compose -f docker-compose.storetest.yaml up --build --exit-code-from store-service

docker-test-user:
	docker compose -f docker-compose.usertest.yaml up --build --exit-code-from user-service

clear:
	docker compose down

test-store: docker-test-store clear

test-user: docker-test-user clear

run:
	docker compose -f docker-compose.yaml up --build 
